package com.example.customviewapp;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity {
    private LinearLayout linearContent;
    private LayoutInflater layoutInflater;
    private RecyclerView recyclerView;
    private ImageView p1, p2, p3;
    private RecyclerView.LayoutManager layoutManager;
    private Context mContext = MainActivity.this;
    private List<TestM> list = new ArrayList<>();
    private TestAdapter adapter;
    int i = 1, i1 = 10;
    private TextView txt_item;
    private FloatingActionButton fab_list;
    private FloatingActionButton fab_pic;
    private Random random;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        initView();
        initList(i, i1);
        layoutInflater = this.getLayoutInflater();
        fab_pic.setOnClickListener((View view) -> {
            View picView = layoutInflater.inflate(R.layout.picture_item_vertical, null);
            p1 = picView.findViewById(R.id.pic_1);
            p2 = picView.findViewById(R.id.pic_2);
            p3 = picView.findViewById(R.id.pic_3);
            random = new Random();
            int resID1 = getResources().getIdentifier(formatremover("drawable/p_" + random.nextInt(8)), "mipmap", getPackageName());
            random = new Random();
            int resID2 = getResources().getIdentifier(formatremover("drawable/p_" + random.nextInt(8)), "mipmap", getPackageName());
            random = new Random();
            int resID3 = getResources().getIdentifier(formatremover("drawable/p_" + random.nextInt(8)), "mipmap", getPackageName());
            p1.setImageResource(resID1);
            p2.setImageResource(resID2);
            p3.setImageResource(resID3);
            linearContent.addView(picView);
        });
        fab_list.setOnClickListener((View view) -> {
            initList(i, i1);
            View rlcView = layoutInflater.inflate(R.layout.recycler_item_horizontal, null);
            recyclerView = rlcView.findViewById(R.id.rcl_horizontal);
            txt_item = rlcView.findViewById(R.id.txt_item);
            txt_item.setText(i + "");
            layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
            adapter = new TestAdapter(list, mContext);
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setAdapter(adapter);
            linearContent.addView(rlcView);
        });
    }

    private void initList(int i, int i1) {
        for (int j = i; j <= i1; j++) {
            this.i = i;
            this.i1 = i1;
            random = new Random();
            int val = random.nextInt(i);
            list.add(new TestM(String.valueOf(val)));
            ;
            i++;
//            i1++;
        }
    }

    private void initView() {
        fab_list = findViewById(R.id.fab_list);
        fab_pic = findViewById(R.id.fab_pic);
        linearContent = findViewById(R.id.linear_content);
        linearContent = findViewById(R.id.linear_content);
    }

    public String formatremover(String withformat) {

        String res = withformat;
        res = res.replace(".png", "");
        res = res.replace(".jpg", "");
        res = res.replace(".jpeg", "");
        res = res.replace(".gif", "");
        res = res.replace(".mp3", "");
        res = res.replace(".mp4", "");
        return res;
    }
}
