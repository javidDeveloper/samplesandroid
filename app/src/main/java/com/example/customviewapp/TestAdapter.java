package com.example.customviewapp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;
import java.util.Random;

public class TestAdapter extends RecyclerView.Adapter<TestAdapter.ViewHolder> {
    List<TestM> list;
    Context mContext;

    public TestAdapter(List<TestM> list, Context mContext) {
        this.list = list;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public TestAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.rlc_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TestAdapter.ViewHolder viewHolder, int position) {
        TestM testM = list.get(position);
        viewHolder.textView.setText(testM.getT());
        Random random = new Random();
        int resID = mContext.getResources().getIdentifier(formatremover("drawable/p_" + random.nextInt(8)), "mipmap", mContext.getPackageName());
        viewHolder.picRlc.setImageResource(resID);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textView;
        ImageView picRlc;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.txt);
            picRlc = itemView.findViewById(R.id.pic_rlc);
        }
    }


    public String formatremover(String withformat) {

        String res = withformat;
        res = res.replace(".png", "");
        res = res.replace(".jpg", "");
        res = res.replace(".jpeg", "");
        res = res.replace(".gif", "");
        res = res.replace(".mp3", "");
        res = res.replace(".mp4", "");
        return res;
    }
}
